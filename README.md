Circle Cutter
=============

This was designed to cut out 40mm circles from PVC report covers
for the inexpensive [Glia Stethoscope](https://github.com/GliaX/Stethoscope)
without spending $125 on a 40mm hole punch.  It uses a standard
#11 X-Acto blade or equivalent, 0.5mm thick, 8mm wide.

## Instructions for Build

The included gcode is a sample only, for one particular printer.
It is included primarily to show print settings that were
successful for me.  Recommended slicer settings are 5 perimeters,
5 top layers, 5 bottom layers, 0.3mm layer height, 30% infill.

Print in PETG or another dimensionally stable plastic; otherwise
you may have to scale slightly to achieve precise dimensions.

Remove any "elephant foot" artifacts from the printed objects,
and make sure that the handle turns smoothly in the template. Sand
off any artifacts until it does so.

Insert a blade into the longer slot with the back edge of the
blade against the inside of the slot, with the tip barely sticking
out the bottom.

Insert an M2.5 (6mm or longer) socket-head screw through the hole in
the blade into the smaller screw hole in the middle of the handle,
and screw it partway in, leaving it loose.

Insert an M3 (8mm or longer) flat-head screw in the larger hole
on the outside of the bottom to tighten the blade retention tab
and hold the blade in place.  Screw it in just far enough to
expand the blade slot, do not tighten it.

Adjust the blade so that the tip sticks out from the bottom of the
handle just far enough to cut your material; 0.5mm to 1mm is good
for normal PVC report covers.  The preferred material is 0.35mm so
just a little deeper than that is best.

Tighten the M3 screw all the way to retain the blade, then tighten
the M2.5 screw all the way.  If the blade moves, loosen the screws,
adjust the blade, and re-tighten the screws until satisfied.

Wrap tape around the exposed section of blade enough to protect
from accidental cuts while in use.

On the top side of the template as printed, spread hot glue
generously across the surface, then immediately squish it firmly onto
parchment paper, and give it a few minutes to cool. Use an X-Acto
or utility knife to trim excess cooled hot glue around the edges.
This will serve to provide an anti-slip surface to keep the circle
you are cutting round.

## Instructions for Use

Place sufficient thickness of sacrificial material under your
work piece to avoid damage to underlying surfaces.  I use a stack
of scratch paper a few mm thick.

Put the PVC report cover or other plastic on top of the paper.

Place the template, glue side down, on top of the report cover. Note
that the hole in the template is larger than the hole that is cut,
and that there are guide lines showing the alignment of the edges
of the 40mm hole.

Set the cutter in the template.

Hold the template firmly while rotating the cutter. Not much pressure
is required. Let the knife do the cutting. You can make multiple
passes until the cut is complete.  The cutting resistance changes
substantially when you have cut through the plastic.

## License

Copyright Michael K Johnson
License: [Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)
